# ICOSA

Numerical library to generate an icosahedron-based geodesic dome on a sphere.

## Homogeneous Mode

![homogeneous](images/icosa_homogeneous.png)

In the homogeneous mode, this library constructs uniform triangular
mesh grids by refining each of 20 icosahedron surfaces with a same
refinment level. Please refer to `icosa_test.c` for a sample code of the
homogeneous mode. Following is a sample usage of the homogeneous mode.

```C
#define MAX_LEVEL (3)
#include "geodesic_dome.h"

struct wavelet_mesh *mesh_array[MAX_LEVEL];  // declaration of a pointer array of the mesh structure
	
setup_wavelet_mesh_all(mesh_array, MAX_LEVEL); // setup the mesh structure up to the 3rd level
```

After setting up the hierarchical mesh strucure, one can find a mesh
grid on a specified level in a specified direction by calling
`lookup_wavelet_mesh_all()`. Following example returns the index of
the 1st level mesh in the direction of ($\theta$, $\phi$) = ($\pi/10$,
$\pi/20$). The identified mesh can be accessed with the returned index.

```C
REAL theta = PI/10.0;
REAL phi = PI/20.0;
int imesh = lookup_wavelet_mesh_all(theta, 	phi, mesh_array, 1);

draw_triangle(mesh_array[1][imesh]);
```


## Adaptive Mode

![adaptive](images/icosa_adaptive.png)

In the adaptive mode, the refined mesh grids can be placed on a given
surface of icosahedra adaptively. Please refer to `icosa_adaptive_test.c`
for a sample code of the adaptive mode.  Following is a sample usage
of the adaptive mode.

```C
#include "geodesic_dome_adaptive.h"

struct wavelet_mesh *base_mesh;

setup_wavelet_mesh_base(&base_mesh); // setup the base mesh grid on the surface of a icosahedron
```

After setting up the base mesh grids, each mesh grid can be refined individually as follows, 

```C
refine_wavelet_mesh(&base_mesh[1]);
refine_wavelet_mesh(&base_mesh[1].next[0]);	
```
Refined meshes of a mesh instance `mesh` can be referred by `mesh.next[]`, while coarser (or parent) mesh is by `mesh.prev`. 
The first line refines the 2nd base mesh grid `base_mesh[1]` and the
second line recursively refines the first mesh grid
`base_mesh[1].next[0]` created in the first line. The finest mesh grid to a specifined direction can be obtained by calling `lookup_wavelet_mesh()` as 

```C
REAL theta = PI/10.0
REAL phi = PI/20.0
struct wavelet_mesh *mesh = lookup_wavelet_mesh(theta, phi, base_mesh) 
```

where `theta` and `phi` are the angles of the direction in spherical coordinate. 

