#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "graph.h"
#include "geodesic_dome.h"

#define MAX_LEVEL (3)

int main(int argc, char **argv)
{
  struct wavelet_mesh **mesh_array;

  mesh_array = (struct wavelet_mesh **) malloc(sizeof(struct wavelet_mesh *)*MAX_LEVEL);
  setup_wavelet_mesh_all(mesh_array, MAX_LEVEL);

#if 0
  for(int i=0;i<20;i++) {
    draw_triangle(&mesh_array[0][i].tri);
    draw_normal(&mesh_array[0][i]);
  }
  for(int i=0;i<80;i++) {
    draw_triangle(&mesh_array[1][i].tri);
    draw_normal(&mesh_array[1][i]);
  }
  for(int i=0;i<320;i++) {
    draw_triangle(&mesh_array[2][i].tri);
    draw_normal(&mesh_array[2][i]);
  }
#endif


#if 0
  REAL theta=2.0*PI/3.0;
  REAL phi = PI/6.0;
  int imesh;

  imesh = lookup_wavelet_mesh_all(theta, phi, mesh_array, 0);
  draw_triangle(&mesh_array[0][imesh].tri);

  imesh = lookup_wavelet_mesh_all(theta, phi, mesh_array, 1);
  draw_triangle(&mesh_array[1][imesh].tri);

  imesh = lookup_wavelet_mesh_all(theta, phi, mesh_array, 2);
  draw_triangle(&mesh_array[2][imesh].tri);
#endif

#if 1
  struct vector vec;
  struct point origin;
  struct point cross_pnt;
  origin.xpos = origin.ypos = origin.zpos = 0.0;

  for(REAL theta=0.0;theta<PI;theta+=PI/128.0) {
    REAL mu = cos(theta);
    for(REAL phi=0.0;phi<2.0*PI;phi+=2.0*PI/256.0) {

      int imesh = lookup_wavelet_mesh_all(theta, phi, mesh_array, 0);
      vec.x = sqrt(1.0-mu*mu)*cos(phi);
      vec.y = sqrt(1.0-mu*mu)*sin(phi);
      vec.z = mu;

      printf("%14.6e %14.6e %14.6e %d\n", vec.x, vec.y, vec.z, imesh);
    }
  }

#endif

  free(mesh_array);
}

