#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>

#include "graph.h"
#include "geodesic_dome_adaptive.h"

void setup_icosa_triangle(struct triangle *tri, struct point *icosa_pnt)
{
  tri[0] = compute_three_point_triangle(icosa_pnt+0,
					icosa_pnt+1,
					icosa_pnt+4);
  tri[1] = compute_three_point_triangle(icosa_pnt+1,
					icosa_pnt+10,
					icosa_pnt+4);
  tri[2] = compute_three_point_triangle(icosa_pnt+10,
					icosa_pnt+5,
					icosa_pnt+4);
  tri[3] = compute_three_point_triangle(icosa_pnt+5,
					icosa_pnt+8,
					icosa_pnt+4);
  tri[4] = compute_three_point_triangle(icosa_pnt+8,
					icosa_pnt+0,
					icosa_pnt+4);
  tri[5] = compute_three_point_triangle(icosa_pnt+0,
					icosa_pnt+1,
					icosa_pnt+6);
  tri[6] = compute_three_point_triangle(icosa_pnt+1,
					icosa_pnt+11,
					icosa_pnt+6);
  tri[7] = compute_three_point_triangle(icosa_pnt+11,
					icosa_pnt+10,
					icosa_pnt+1);
  tri[8] = compute_three_point_triangle(icosa_pnt+10,
					icosa_pnt+3,
					icosa_pnt+11);
  tri[9] = compute_three_point_triangle(icosa_pnt+3,
					icosa_pnt+5,
					icosa_pnt+10);
  tri[10] = compute_three_point_triangle(icosa_pnt+5,
					 icosa_pnt+2,
					 icosa_pnt+3);
  tri[11] = compute_three_point_triangle(icosa_pnt+2,
					 icosa_pnt+8,
					 icosa_pnt+5);
  tri[12] = compute_three_point_triangle(icosa_pnt+8,
					 icosa_pnt+9,
					 icosa_pnt+2);
  tri[13] = compute_three_point_triangle(icosa_pnt+9,
					 icosa_pnt+0,
					 icosa_pnt+8);
  tri[14] = compute_three_point_triangle(icosa_pnt+0,
					 icosa_pnt+6,
					 icosa_pnt+9);
  tri[15] = compute_three_point_triangle(icosa_pnt+11,
					 icosa_pnt+7,
					 icosa_pnt+6);
  tri[16] = compute_three_point_triangle(icosa_pnt+7,
					 icosa_pnt+3,
					 icosa_pnt+11);
  tri[17] = compute_three_point_triangle(icosa_pnt+3,
					 icosa_pnt+2,
					 icosa_pnt+7);
  tri[18] = compute_three_point_triangle(icosa_pnt+2,
					 icosa_pnt+9,
					 icosa_pnt+7);
  tri[19] = compute_three_point_triangle(icosa_pnt+9,
					 icosa_pnt+6,
					 icosa_pnt+7);
}

void setup_icosa_pnt(struct point *pnt) 
{
  REAL golden_ratio = 0.5*(1.0+sqrt(5.0));
  REAL norm = sqrt(1.0+SQR(golden_ratio));

  pnt[0].xpos = 1.0; pnt[0].ypos = golden_ratio; pnt[0].zpos=0.0;
  pnt[1].xpos =-1.0; pnt[1].ypos = golden_ratio; pnt[1].zpos=0.0;
  pnt[2].xpos = 1.0; pnt[2].ypos =-golden_ratio; pnt[2].zpos=0.0;
  pnt[3].xpos =-1.0; pnt[3].ypos =-golden_ratio; pnt[3].zpos=0.0;

  pnt[4].xpos = 0.0; pnt[4].ypos = 1.0; pnt[4].zpos= golden_ratio;
  pnt[5].xpos = 0.0; pnt[5].ypos =-1.0; pnt[5].zpos= golden_ratio;
  pnt[6].xpos = 0.0; pnt[6].ypos = 1.0; pnt[6].zpos=-golden_ratio;
  pnt[7].xpos = 0.0; pnt[7].ypos =-1.0; pnt[7].zpos=-golden_ratio;

  pnt[ 8].xpos = golden_ratio; pnt[ 8].ypos = 0.0; pnt[ 8].zpos= 1.0;
  pnt[ 9].xpos = golden_ratio; pnt[ 9].ypos = 0.0; pnt[ 9].zpos=-1.0;
  pnt[10].xpos =-golden_ratio; pnt[10].ypos = 0.0; pnt[10].zpos= 1.0;
  pnt[11].xpos =-golden_ratio; pnt[11].ypos = 0.0; pnt[11].zpos=-1.0;

  for(int i=0;i<NPNT_ICOSA;i++) {
    pnt[i].xpos /= norm;
    pnt[i].ypos /= norm;
    pnt[i].zpos /= norm;
  }
}

void draw_normal(struct wavelet_mesh *mesh)
{
  REAL xcent = (mesh->tri.pnt[0].xpos + mesh->tri.pnt[1].xpos + mesh->tri.pnt[2]
.xpos)/3.0;
  REAL ycent = (mesh->tri.pnt[0].ypos + mesh->tri.pnt[1].ypos + mesh->tri.pnt[2].ypos)/3.0;
  REAL zcent = (mesh->tri.pnt[0].zpos + mesh->tri.pnt[1].zpos + mesh->tri.pnt[2].zpos)/3.0;
  printf("%14.6e %14.6e %14.6e\n", xcent, ycent, zcent);

  struct vector norm;
  REAL sintheta;
  REAL sinphi, cosphi;
  sintheta = sin(mesh->theta);
  sinphi = sin(mesh->phi);
  cosphi = cos(mesh->phi);
  norm.x = cosphi*sintheta;
  norm.y = sinphi*sintheta;
  norm.z = cos(mesh->theta);
  xcent += norm.x;
  ycent += norm.y;
  zcent += norm.z;
  printf("%14.6e %14.6e %14.6e\n", xcent, ycent, zcent);
  printf("\n");
  printf("\n");
}

void draw_triangle_vertex(struct triangle *tri)
{
  printf("%14.6e %14.6e %14.6e\n", tri->pnt[0].xpos, tri->pnt[0].ypos, tri->pnt[0].zpos);
  printf("%14.6e %14.6e %14.6e\n", tri->pnt[1].xpos, tri->pnt[1].ypos, tri->pnt[1].zpos);
  printf("%14.6e %14.6e %14.6e\n", tri->pnt[2].xpos, tri->pnt[2].ypos, tri->pnt[2].zpos);
}

void draw_triangle(struct triangle *tri)
{
  struct vector v1 = relative_vector(&tri->pnt[1], &tri->pnt[0]);
  struct vector v2 = relative_vector(&tri->pnt[2], &tri->pnt[0]);

  printf("\n");
  printf("%14.6e %14.6e %14.6e\n", tri->pnt[0].xpos, tri->pnt[0].ypos, tri->pnt[0].zpos);
  printf("%14.6e %14.6e %14.6e\n", tri->pnt[1].xpos, tri->pnt[1].ypos, tri->pnt[1].zpos);
  printf("%14.6e %14.6e %14.6e\n", tri->pnt[2].xpos, tri->pnt[2].ypos, tri->pnt[2].zpos);
  printf("%14.6e %14.6e %14.6e\n", tri->pnt[0].xpos, tri->pnt[0].ypos, tri->pnt[0].zpos);
  printf("\n");
#if 0
  for(float f1=0.05;f1<1.0;f1+=0.05){
    for(float f2=0.05;f2<1.0;f2+=0.05){
      if(f1+f2<1.0) {
        float x = tri->pnt[0].xpos + f1*v1.x +f2*v2.x;
        float y = tri->pnt[0].ypos + f1*v1.y +f2*v2.y;
        float z = tri->pnt[0].zpos + f1*v1.z +f2*v2.z;
        printf("%14.6e %14.6e %14.6e\n", x, y, z);
        printf("\n");
      }
    }
  }
#endif
  
}

void draw_mesh_vertex(struct wavelet_mesh *mesh)
{

  if(mesh->next != NULL) {
    for(int i=0;i<4;i++) {
      draw_mesh_vertex(&(mesh->next[i]));
    }
  }else{
    draw_triangle_vertex(&(mesh->tri));
  }
}

void draw_mesh(struct wavelet_mesh *mesh) 
{
  draw_triangle(&(mesh->tri));
  if(mesh->next != NULL) {
    for(int i=0;i<4;i++) {
      draw_mesh(&(mesh->next[i]));
    }
  }
}


void decompose_wavelet_mesh(struct wavelet_mesh *mesh, 
			    struct wavelet_mesh *next_mesh)
{
  struct point midpnt_01;
  struct point midpnt_12;
  struct point midpnt_20;
  struct point origin;

  origin.xpos = origin.ypos = origin.zpos = 0.0;
  midpnt_01.xpos = 0.5*(mesh->tri.pnt[0].xpos + mesh->tri.pnt[1].xpos);
  midpnt_01.ypos = 0.5*(mesh->tri.pnt[0].ypos + mesh->tri.pnt[1].ypos);
  midpnt_01.zpos = 0.5*(mesh->tri.pnt[0].zpos + mesh->tri.pnt[1].zpos);
  midpnt_12.xpos = 0.5*(mesh->tri.pnt[1].xpos + mesh->tri.pnt[2].xpos);
  midpnt_12.ypos = 0.5*(mesh->tri.pnt[1].ypos + mesh->tri.pnt[2].ypos);
  midpnt_12.zpos = 0.5*(mesh->tri.pnt[1].zpos + mesh->tri.pnt[2].zpos);
  midpnt_20.xpos = 0.5*(mesh->tri.pnt[2].xpos + mesh->tri.pnt[0].xpos);
  midpnt_20.ypos = 0.5*(mesh->tri.pnt[2].ypos + mesh->tri.pnt[0].ypos);
  midpnt_20.zpos = 0.5*(mesh->tri.pnt[2].zpos + mesh->tri.pnt[0].zpos);

  struct vector vm01, vm12, vm20;
  vm01 = relative_vector(&midpnt_01, &origin);
  vm12 = relative_vector(&midpnt_12, &origin);
  vm20 = relative_vector(&midpnt_20, &origin);

  vm01 = normalize(&vm01);
  vm12 = normalize(&vm12);
  vm20 = normalize(&vm20);

  struct surface surf_temp;
  REAL xc, yc, zc;
  struct vector norm_vect;
  //
  next_mesh[0].tri.pnt[0] = mesh->tri.pnt[0];

  next_mesh[0].tri.pnt[1].xpos = origin.xpos + vm01.x;
  next_mesh[0].tri.pnt[1].ypos = origin.ypos + vm01.y;
  next_mesh[0].tri.pnt[1].zpos = origin.zpos + vm01.z;

  next_mesh[0].tri.pnt[2].xpos = origin.xpos + vm20.x;
  next_mesh[0].tri.pnt[2].ypos = origin.ypos + vm20.y;
  next_mesh[0].tri.pnt[2].zpos = origin.zpos + vm20.z;

  xc = (next_mesh[0].tri.pnt[0].xpos + 
	next_mesh[0].tri.pnt[1].xpos + 
	next_mesh[0].tri.pnt[2].xpos)/3.0;
  yc = (next_mesh[0].tri.pnt[0].ypos + 
	next_mesh[0].tri.pnt[1].ypos + 
	next_mesh[0].tri.pnt[2].ypos)/3.0;
  zc = (next_mesh[0].tri.pnt[0].zpos + 
	next_mesh[0].tri.pnt[1].zpos + 
	next_mesh[0].tri.pnt[2].zpos)/3.0;

  norm_vect.x = xc;
  norm_vect.y = yc;
  norm_vect.z = zc;
  norm_vect = normalize(&norm_vect);

  next_mesh[0].theta = acos(norm_vect.z);
  next_mesh[0].phi = atan2(norm_vect.y, norm_vect.x);
  next_mesh[0].next = NULL;
  next_mesh[0].prev = mesh;
  next_mesh[0].base = 0;

  //
  next_mesh[1].tri.pnt[0] = next_mesh[0].tri.pnt[1];

  next_mesh[1].tri.pnt[1] = mesh->tri.pnt[1];

  next_mesh[1].tri.pnt[2].xpos = origin.xpos + vm12.x;
  next_mesh[1].tri.pnt[2].ypos = origin.ypos + vm12.y;
  next_mesh[1].tri.pnt[2].zpos = origin.zpos + vm12.z;

  xc = (next_mesh[1].tri.pnt[0].xpos + 
	next_mesh[1].tri.pnt[1].xpos + 
	next_mesh[1].tri.pnt[2].xpos)/3.0;
  yc = (next_mesh[1].tri.pnt[0].ypos + 
	next_mesh[1].tri.pnt[1].ypos + 
	next_mesh[1].tri.pnt[2].ypos)/3.0;
  zc = (next_mesh[1].tri.pnt[0].zpos + 
	next_mesh[1].tri.pnt[1].zpos + 
	next_mesh[1].tri.pnt[2].zpos)/3.0;

  norm_vect.x = xc;
  norm_vect.y = yc;
  norm_vect.z = zc;
  norm_vect = normalize(&norm_vect);

  next_mesh[1].theta = acos(norm_vect.z);
  next_mesh[1].phi = atan2(norm_vect.y, norm_vect.x);
  next_mesh[1].next = NULL;
  next_mesh[1].prev = mesh;
  next_mesh[1].base = 0;

  //
  next_mesh[2].tri.pnt[0] = next_mesh[1].tri.pnt[2];
  next_mesh[2].tri.pnt[1] = next_mesh[0].tri.pnt[2];
  next_mesh[2].tri.pnt[2] = next_mesh[0].tri.pnt[1];

  xc = (next_mesh[2].tri.pnt[0].xpos + 
	next_mesh[2].tri.pnt[1].xpos + 
	next_mesh[2].tri.pnt[2].xpos)/3.0;
  yc = (next_mesh[2].tri.pnt[0].ypos + 
	next_mesh[2].tri.pnt[1].ypos + 
	next_mesh[2].tri.pnt[2].ypos)/3.0;
  zc = (next_mesh[2].tri.pnt[0].zpos + 
	next_mesh[2].tri.pnt[1].zpos + 
	next_mesh[2].tri.pnt[2].zpos)/3.0;

  norm_vect.x = xc;
  norm_vect.y = yc;
  norm_vect.z = zc;
  norm_vect = normalize(&norm_vect);

  next_mesh[2].theta = acos(norm_vect.z);
  next_mesh[2].phi = atan2(norm_vect.y, norm_vect.x);
  next_mesh[2].next = NULL;
  next_mesh[2].prev = mesh;
  next_mesh[2].base = 0;

  // 
  next_mesh[3].tri.pnt[0] = next_mesh[0].tri.pnt[2];
  next_mesh[3].tri.pnt[1] = next_mesh[1].tri.pnt[2];
  next_mesh[3].tri.pnt[2] = mesh->tri.pnt[2];

  xc = (next_mesh[3].tri.pnt[0].xpos + 
	next_mesh[3].tri.pnt[1].xpos + 
	next_mesh[3].tri.pnt[2].xpos)/3.0;
  yc = (next_mesh[3].tri.pnt[0].ypos + 
	next_mesh[3].tri.pnt[1].ypos + 
	next_mesh[3].tri.pnt[2].ypos)/3.0;
  zc = (next_mesh[3].tri.pnt[0].zpos + 
	next_mesh[3].tri.pnt[1].zpos + 
	next_mesh[3].tri.pnt[2].zpos)/3.0;

  norm_vect.x = xc;
  norm_vect.y = yc;
  norm_vect.z = zc;
  norm_vect = normalize(&norm_vect);

  next_mesh[3].theta = acos(norm_vect.z);
  next_mesh[3].phi = atan2(norm_vect.y, norm_vect.x);
  next_mesh[3].next = NULL;
  next_mesh[3].prev = mesh;
  next_mesh[3].base = 0;

}


void refine_wavelet_mesh(struct wavelet_mesh *mesh)
{
  mesh->next = (struct wavelet_mesh *)malloc(sizeof(struct wavelet_mesh)*4);

  decompose_wavelet_mesh(mesh, mesh->next);
}

void destroy_submesh(struct wavelet_mesh *mesh) 
{
  /* recursively destroy the refined mesh in THIS mesh */
  for(int i=0;i<4;i++) {
    if((mesh->next[i]).next!=NULL) {
      destroy_submesh(mesh->next+i);
    }
  }
  
  free(mesh->next);
  
  mesh->next = NULL;
}

struct wavelet_mesh* lookup_wavelet_mesh_next(REAL theta, REAL phi, 
					      struct wavelet_mesh *mesh)
{
  struct point origin;
  origin.xpos = origin.ypos = origin.zpos = 0.0;  

  REAL mu = cos(theta);
  REAL sintheta = sqrt(1.0-mu*mu);

  struct vector vec;
  vec.x = sintheta*cos(phi);
  vec.y = sintheta*sin(phi);
  vec.z = mu;
  
  for(int i=0;i<4;i++) {
    int is_hit = bool_shoot_triangle(&origin, &vec, &mesh[i].tri);
    if(is_hit) {
      struct wavelet_mesh *tmp;
      if(mesh[i].next != NULL) {
	tmp = lookup_wavelet_mesh_next(theta, phi, mesh[i].next);
	return tmp;
      }else{
	tmp = mesh+i;
	return tmp;
      }
    }
  }
}

struct wavelet_mesh* lookup_wavelet_mesh(REAL theta, REAL phi, 
					 struct wavelet_mesh *base_mesh)
{
  struct point origin;
  origin.xpos = origin.ypos = origin.zpos = 0.0;  

  REAL mu = cos(theta);
  REAL sintheta = sqrt(1.0-mu*mu);

  struct vector vec;
  vec.x = sintheta*cos(phi);
  vec.y = sintheta*sin(phi);
  vec.z = mu;

  for(int i=0;i<NSURF_ICOSA;i++) {
    int is_hit = bool_shoot_triangle(&origin, &vec, &base_mesh[i].tri);
    if(is_hit) {
      struct wavelet_mesh *tmp;
      if(base_mesh[i].next != NULL) {
	tmp = lookup_wavelet_mesh_next(theta, phi, base_mesh[i].next);
	return tmp;
      }else{
	tmp = base_mesh+i;
	return tmp;
      }
    }
  }
}

void setup_wavelet_mesh_base(struct wavelet_mesh **base_mesh)
{
  static struct point icosa_pnt[NPNT_ICOSA];
  static struct triangle icosa_tri[NSURF_ICOSA];

  setup_icosa_pnt(icosa_pnt);
  setup_icosa_triangle(icosa_tri, icosa_pnt);

  *base_mesh = (struct wavelet_mesh *) malloc(sizeof(struct wavelet_mesh)*20);

  for(int i=0;i<NSURF_ICOSA;i++) {
    (*base_mesh)[i].tri = icosa_tri[i];

    REAL xcent = (icosa_tri[i].pnt[0].xpos + 
		  icosa_tri[i].pnt[1].xpos + 
		  icosa_tri[i].pnt[2].xpos)/3.0;
    REAL ycent = (icosa_tri[i].pnt[0].ypos + 
		  icosa_tri[i].pnt[1].ypos + 
		  icosa_tri[i].pnt[2].ypos)/3.0;
    REAL zcent = (icosa_tri[i].pnt[0].zpos + 
		  icosa_tri[i].pnt[1].zpos + 
		  icosa_tri[i].pnt[2].zpos)/3.0;
    struct vector norm;
    norm.x = xcent;
    norm.y = ycent;
    norm.z = zcent;
    norm = normalize(&norm);
    (*base_mesh)[i].theta = acos(norm.z);
    (*base_mesh)[i].phi = atan2(norm.y,norm.x);
    (*base_mesh)[i].next = NULL;
    (*base_mesh)[i].prev = NULL;
    (*base_mesh)[i].base = 1;
  }

}
