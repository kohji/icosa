#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "geodesic_dome.h"

#define MAX_LEVEL (3)

int main(int argc, char **argv)
{
  //  struct wavelet_mesh **mesh_array;
  //  mesh_array = (struct wavelet_mesh **) malloc(sizeof(struct wavelet_mesh *)*MAX_LEVEL);
  
  struct wavelet_mesh *mesh_array[MAX_LEVEL];
  setup_wavelet_mesh_all(mesh_array, MAX_LEVEL);

  FILE *test_fp;

  test_fp = fopen("level0", "w");
  for(REAL theta=0.0;theta<PI;theta+=PI/128.0) {
    for(REAL phi=0.0;phi<2.0*PI;phi+=2.0*PI/256.0) {
      REAL x, y, z;

      REAL mu = cos(theta);

      int imesh = lookup_wavelet_mesh_all(theta, phi, mesh_array, 0);
      x = sqrt(1.0-mu*mu)*cos(phi);
      y = sqrt(1.0-mu*mu)*sin(phi);
      z = mu;

      fprintf(test_fp, "%14.6e %14.6e %14.6e %d\n", x, y, z, imesh);
    }
  }

  test_fp = fopen("level1", "w");
  for(REAL theta=0.0;theta<PI;theta+=PI/128.0) {
    for(REAL phi=0.0;phi<2.0*PI;phi+=2.0*PI/256.0) {
      REAL x, y, z;

      REAL mu = cos(theta);

      int imesh = lookup_wavelet_mesh_all(theta, phi, mesh_array, 1);
      x = sqrt(1.0-mu*mu)*cos(phi);
      y = sqrt(1.0-mu*mu)*sin(phi);
      z = mu;

      fprintf(test_fp, "%14.6e %14.6e %14.6e %d\n", x, y, z, imesh);
    }
  }

  test_fp = fopen("level2", "w");
  for(REAL theta=0.0;theta<PI;theta+=PI/128.0) {
    for(REAL phi=0.0;phi<2.0*PI;phi+=2.0*PI/256.0) {
      REAL x, y, z;

      REAL mu = cos(theta);

      int imesh = lookup_wavelet_mesh_all(theta, phi, mesh_array, 2);
      x = sqrt(1.0-mu*mu)*cos(phi);
      y = sqrt(1.0-mu*mu)*sin(phi);
      z = mu;

      fprintf(test_fp, "%14.6e %14.6e %14.6e %d\n", x, y, z, imesh);
    }
  }

}
