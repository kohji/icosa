#pragma once
#include "graph.h"

#define NPNT_ICOSA (12)
#define NSURF_ICOSA (20)

struct wavelet_mesh {
  struct triangle tri;
  struct wavelet_mesh *next;
  struct wavelet_mesh *prev;
  REAL theta, phi;
  int base;
};

void setup_icosa_triangle(struct triangle*, struct point*);
void setup_icosa_surf(struct surface*, struct point*);
void setup_icosa_pnt(struct point*);
void draw_normal(struct wavelet_mesh*);
void draw_triangle_vertex(struct triangle*);
void draw_triangle(struct triangle*);
void draw_mesh_vertex(struct wavelet_mesh*);
void draw_mesh(struct wavelet_mesh*);
void decompose_wavelet_mesh(struct wavelet_mesh*, struct wavelet_mesh*);
void refine_wavelet_mesh(struct wavelet_mesh *);
void destroy_submesh(struct wavelet_mesh *);
struct wavelet_mesh* lookup_wavelet_mesh_next(REAL, REAL, struct wavelet_mesh*);
struct wavelet_mesh* lookup_wavelet_mesh(REAL, REAL, struct wavelet_mesh*);
void setup_wavelet_mesh_base(struct wavelet_mesh **);
