#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import mpl_toolkits.mplot3d as a3

def read_trianguler_vertex(x, y, z, filename):
    file = open(filename, 'r')
    indx=0
    for line in file:
        indx += 1
        
    file.seek(0,0)
    
    x.resize(indx,refcheck=False)
    y.resize(indx,refcheck=False)
    z.resize(indx,refcheck=False)

    indx=0

    for line in file:
        data = line.split()
        x[indx]=data[0]
        y[indx]=data[1]
        z[indx]=data[2]
        indx+=1

    return int(indx/3)
    
x=np.empty(1)
y=np.empty(1)
z=np.empty(1)

nvertex = read_trianguler_vertex(x, y, z, 'out.dat')

ax = a3.Axes3D(plt.figure())

for iv in range(nvertex):
    vtx = []
    vtx.append([x[3*iv],y[3*iv], z[3*iv]])
    vtx.append([x[3*iv+1],y[3*iv+1], z[3*iv+1]])
    vtx.append([x[3*iv+2],y[3*iv+2], z[3*iv+2]])
    tri = a3.art3d.Poly3DCollection([vtx])
    tri.set_color(colors.rgb2hex(np.random.rand(4)))
    tri.set_edgecolor('k')
    ax.add_collection3d(tri)
    
ax.xaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
ax.yaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
ax.zaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
ax.set_xlim(-1.0, 1.0)
ax.set_ylim(-1.0, 1.0)
ax.set_zlim(-1.0, 1.0)
ax.grid(b=None)
ax.set_axis_off()
plt.show()


