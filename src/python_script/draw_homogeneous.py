#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import numpy as np
from mpl_toolkits.mplot3d import Axes3D

def read_icosa_data(x,y,z,mesh_id,filename):
    file = open(filename, 'r')
    indx=0
    for line in file:
        indx+=1

    x.resize(indx, refcheck=False)
    y.resize(indx, refcheck=False)
    z.resize(indx, refcheck=False)
    mesh_id.resize(indx, refcheck=False)

    indx=0
    file.seek(0,0)

    for line in file:
        data = line.split()
        x[indx] = data[0]
        y[indx] = data[1]
        z[indx] = data[2]
        mesh_id[indx] = int(data[3])
        indx += 1
    
    file.close()
    return indx

filename_list=['level0', 'level1', 'level2']
title_list=['level 0', 'level 1', 'level 2']
x=np.empty(1)
y=np.empty(1)
z=np.empty(1)
mesh_id=np.empty(1,dtype=np.int32)

cm_name = 'jet' # B->G->R
cm = plt.get_cmap(cm_name)



fig = plt.figure(figsize=(18,5))
for ilvl in range(len(filename_list)):
    ax = fig.add_subplot(1,3,ilvl+1, projection='3d')


    npnts = read_icosa_data(x,y,z,mesh_id,filename_list[ilvl])
    x *= 1.2
    y *= 1.2
    z *= 1.2
    id_ = set(mesh_id)

    col_seq=[]
    for id in id_:
        col_id = int(float(id)/len(id_)*cm.N)
        col_seq.append(colors.rgb2hex([cm(col_id)[0], cm(col_id)[1], cm(col_id)[2], cm(col_id)[3]]))

    for id in id_:
        col_id = np.random.randint(0,len(id_))
        ax.scatter(x[mesh_id==id], y[mesh_id==id], z[mesh_id==id],c=col_seq[col_id],s=3)
    ax.set_title(title_list[ilvl],fontsize=16)

    ax.xaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
    ax.yaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
    ax.zaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
    ax.set_xlim(-1.0, 1.0)
    ax.set_ylim(-1.0, 1.0)
    ax.set_zlim(-1.0, 1.0)
    ax.grid(b=None)
    ax.set_axis_off()
    
plt.show()
