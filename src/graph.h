#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <float.h>

typedef double REAL;
typedef unsigned int UINT;

#define SQR(x) ((x)*(x))
#define NORM2(x,y,z) (SQR(x)+SQR(y)+SQR(z))

#define PI (3.1415926535897932384626433)
#define TINY_CONST (1.0e-10)

struct point
{
  UINT indx;
  REAL xpos, ypos, zpos;
};

struct vector
{
  REAL x,y,z;
};

struct line
{
  struct point pnt;
  struct vector vect;
};

struct surface
{
  struct point cent;
  struct vector norm_vect;
};

struct triangle
{
  struct point pnt[3];
};

// prototypes

struct vector relative_vector(struct point *p0, struct point *p1);
struct line define_line(struct point *p, struct vector *v);
struct line connecting_line(struct point *p0, struct point *p1);
REAL dot_product(struct vector *v1, struct vector *v2);
struct vector cross_product(struct vector *v1, struct vector *v2);
struct vector normalize(struct vector *v);
struct surface compute_bisector_surface(struct point *p1, struct point *p2);
struct surface compute_three_point_surface(struct point *p0, 
					   struct point *p1, 
					   struct point *p2);
struct triangle compute_three_point_triangle(struct point *p0, 
					     struct point *p1, 
					     struct point *p2);
struct point compute_crosspoint_three_surface(struct surface *s0, 
					      struct surface *s1, 
					      struct surface *s2);
REAL compute_crosspoint_line_surface(struct point *p, 
                                     struct line *l, struct surface *s);
REAL compute_distance_point_surface(struct point *pnt,
                                    struct surface *s);
REAL compute_distance_point_line(struct point *pnt, struct line *l);
int bool_shoot_triangle(struct point *, struct vector *, struct triangle *);
