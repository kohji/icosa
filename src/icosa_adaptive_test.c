#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "geodesic_dome_adaptive.h"

int main(int argc, char **argv)
{
  struct wavelet_mesh *base_mesh;

  setup_wavelet_mesh_base(&base_mesh);

  refine_wavelet_mesh(&base_mesh[0]);
  refine_wavelet_mesh(&base_mesh[0].next[1]);

#if 1
  for(int i=0;i<NSURF_ICOSA;i++) {
    draw_mesh_vertex(&(base_mesh[i]));
  }
#endif
  
#if 0
  for(REAL theta=0.0;theta<PI;theta+=PI/8.0) {
    for(REAL phi=0.0;phi<2.0*PI;phi+=2.0*PI/32.0) {
      REAL x, y, z;

      REAL mu = cos(theta);

      struct wavelet_mesh *mesh;
      mesh = lookup_wavelet_mesh(theta, phi, base_mesh);
      x = sqrt(1.0-mu*mu)*cos(phi);
      y = sqrt(1.0-mu*mu)*sin(phi);
      z = mu;

      printf("\n%14.6e %14.6e %14.6e\n", x, y, z);
      draw_triangle(&(mesh->tri));
    }
  }
#endif

  destroy_submesh(&base_mesh[0]);
}
