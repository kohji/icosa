#!/bin/bash

rm -rf level0_seq.plot
rm -rf level1_seq.plot
rm -rf level2_seq.plot

echo "set view equal xyz" >> level0_seq.plot
echo "set xlabel 'X'" >> level0_seq.plot
echo "set ylabel 'Y'" >> level0_seq.plot
echo "set zlabel 'Z'" >> level0_seq.plot
echo "set xrange [-1.1:1.1]" >> level0_seq.plot
echo "set yrange [-1.1:1.1]" >> level0_seq.plot
echo "set zrange [-1.1:1.1]" >> level0_seq.plot
for i in {0..20}
do
echo "splot \"level0\" u 1:2:(\$4==${i} ? \$3 : 1/0) notitle" >> level0_seq.plot
echo "pause -1 \"Hit return to proceed\" " >> level0_seq.plot
done

echo "set view equal xyz" >> level1_seq.plot
echo "set xlabel 'X'" >> level1_seq.plot
echo "set ylabel 'Y'" >> level1_seq.plot
echo "set zlabel 'Z'" >> level1_seq.plot
echo "set xrange [-1.1:1.1]" >> level1_seq.plot
echo "set yrange [-1.1:1.1]" >> level1_seq.plot
echo "set zrange [-1.1:1.1]" >> level1_seq.plot
for i in {0..80}
do
echo "splot \"level1\" u 1:2:(\$4==${i} ? \$3 : 1/0) notitle" >> level1_seq.plot
echo "pause -1 \"Hit return to proceed\" " >> level1_seq.plot
done


echo "set view equal xyz" >> level2_seq.plot
echo "set xlabel 'X'" >> level2_seq.plot
echo "set ylabel 'Y'" >> level2_seq.plot
echo "set zlabel 'Z'" >> level2_seq.plot
echo "set xrange [-1.1:1.1]" >> level2_seq.plot
echo "set yrange [-1.1:1.1]" >> level2_seq.plot
echo "set zrange [-1.1:1.1]" >> level2_seq.plot
for i in {0..320}
do
echo "splot \"level2\" u 1:2:(\$4==${i} ? \$3 : 1/0) notitle" >> level2_seq.plot
echo "pause -1 \"Hit return to proceed\" " >> level2_seq.plot
done



