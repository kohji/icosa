#!/bin/bash

rm -rf level0_all.plot
rm -rf level1_all.plot
rm -rf level2_all.plot

echo "set view equal xyz" >> level0_all.plot
echo "set xlabel 'X'" >> level0_all.plot
echo "set ylabel 'Y'" >> level0_all.plot
echo "set zlabel 'Z'" >> level0_all.plot
echo -n "splot " >> level0_all.plot
for i in {0..20}
do
echo -n "\"level0\" u 1:2:(\$4==${i} ? \$3 : 1/0) notitle, " >> level0_all.plot
done

echo "set view equal xyz" >> level1_all.plot
echo "set xlabel 'X'" >> level1_all.plot
echo "set ylabel 'Y'" >> level1_all.plot
echo "set zlabel 'Z'" >> level1_all.plot
echo -n "splot " >> level1_all.plot
for i in {0..20}
do
echo -n "\"level1\" u 1:2:(\$4==${i} ? \$3 : 1/0) notitle, " >> level1_all.plot
done

echo "set view equal xyz" >> level2_all.plot
echo "set xlabel 'X'" >> level2_all.plot
echo "set ylabel 'Y'" >> level2_all.plot
echo "set zlabel 'Z'" >> level2_all.plot
echo -n "splot " >> level2_all.plot
for i in {0..20}
do
echo -n "\"level2\" u 1:2:(\$4==${i} ? \$3 : 1/0) notitle, " >> level2_all.plot
done
