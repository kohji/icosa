#pragma once
#include "graph.h"

#define NPNT_ICOSA (12)
#define NSURF_ICOSA (20)

struct wavelet_mesh {
  struct triangle tri;
  REAL theta, phi;
};

void setup_icosa_triangle(struct triangle*, struct point*);
void setup_icosa_surf(struct surface*, struct point*);
void setup_icosa_pnt(struct point*);
void draw_normal(struct wavelet_mesh*);
void draw_triangle(struct triangle*);
void decompose_wavelet_mesh(struct wavelet_mesh*, struct wavelet_mesh*);
void refine_wavelet_mesh(struct wavelet_mesh*, int, struct wavelet_mesh*);
void setup_base_wavelet_mesh(struct wavelet_mesh*, struct triangle*);
int lookup_wavelet_mesh_level(REAL, REAL, struct wavelet_mesh*, int, int);
int lookup_wavelet_mesh_all(REAL, REAL, struct wavelet_mesh**,  int);
void setup_wavelet_mesh_all(struct wavelet_mesh **, int);
